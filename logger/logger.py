import os
import json
import logging.config

import graypy

from datetime import datetime
app_logger = None


class BraceString(str):
    def __mod__(self, other):
        return self.format(*other)
    def __str__(self):
        return self


class StyleAdapter(logging.LoggerAdapter):

    def __init__(self, logger, extra=None):
        super(StyleAdapter, self).__init__(logger, extra)

    def process(self, msg, kwargs):
        if kwargs.pop('style', "%") == "{":  # optional
            msg = BraceString(msg)
        return msg, kwargs

class CustomLogger:

    logger = None

    instanciated_logger = []

    def __init__(self):
        self.logger = None


    def get_logger(self, name):

        if name is None:
            self.logger = logging.getLogger(name)
            return self.logger

        if name not in self.instanciated_logger:
            self.setup_logging(name=name)
            self.instanciated_logger.append(name)

        self.logger = logging.getLogger(name)

        return self.logger

    def setup_logging(self, name="service_tasks", default_path = 'logging.json'):

        ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
        path = os.path.join(ROOT_DIR.replace('/logger', '')+'/logger', default_path)
        print path

        source_directory = path.split('/')[3]

        with open(path, 'rt') as f:
            config = json.load(f)

        info_log_filename = config['handlers']['info_file_handler']['filename']
        error_file_name = config['handlers']['info_file_handler']['filename']

        info_log_folder = '/'.join(info_log_filename.split('/')[:len(info_log_filename.split('/'))-1])
        error_log_folder = '/'.join(error_file_name.split('/')[:len(error_file_name.split('/'))-1])


        if not os.path.exists(info_log_folder + "/"+source_directory+"/"):
            os.makedirs(info_log_folder + "/"+source_directory+"/")


        config['handlers']['info_file_handler']['filename'] = info_log_folder + "/"+source_directory+"/" + name + "_info.log"

        config['handlers']['error_file_handler']['filename'] = info_log_folder + "/"+source_directory+"/"  + name+ "_error.log"

        logging.config.dictConfig(config)


        # if value:
        #     path = value
        # if os.path.exists(path):
        #
        # else:
        #     logging.basicConfig(level=default_level)

    def get_conf(self, default_path='logging.json'):
        ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
        path = os.path.join(ROOT_DIR.replace('/logger', ''), default_path)
        with open(path, 'rt') as f:
            config = json.load(f)
        return config

logger = CustomLogger()

my_cool = "10"

def get_logger(name=None):
    global app_logger

    if app_logger is None:
        my_logger = logger.get_logger(name)
        print "Setting gelf tcp handler"
        # handler = GelfTcpHandler(host='52.66.71.150', port=12207, include_extra_fields=True)
        handler = graypy.GELFHandler('52.66.71.150', 12201)
        # my_logger.propagate = False
        my_logger.addHandler(handler)

        app_logger = my_logger

    return app_logger
    
def format(*args):
    return ' '.join(str(i) if i is not None else '' for i in args)



def get_conf():
    return logger.get_conf()

