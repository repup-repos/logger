def format(*args):
    return ' '.join(str(i) if i is not None else '' for i in args)
